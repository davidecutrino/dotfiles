set encoding=utf-8
set number
set ft=javascript
let NERDTreeShowHidden=1

set nocompatible " Enable all the 'improved' features of Vim.
                 " This is required for Vundle
		 
filetype off " Also required for Vundle.
             " Prevents potential side-effects
             " from system ftdetects scripts
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'

" Other plugins go here:

"*****************************:
Plugin 'scrooloose/nerdTree'
autocmd vimenter * NERDTree

Plugin 'Xuyuanp/nerdtree-git-plugin'
let g:NERDTreeIndicatorMapCustom = {
    \ "Modified"  : "✹",
    \ "Staged"    : "✚",
    \ "Untracked" : "✭",
    \ "Renamed"   : "➜",
    \ "Unmerged"  : "═",
    \ "Deleted"   : "✖",
    \ "Dirty"     : "✗",
    \ "Clean"     : "✔︎",
    \ "Unknown"   : "?"
    \ }

Bundle 'altercation/vim-colors-solarized'
"colorscheme solarized
se t_Co=16
let g:solarized_termcolors=256  
set background=dark  
colorscheme solarized

Bundle 'Lokaltog/vim-powerline'
let g:Powerline_symbols = 'fancy'
set laststatus=2
set termencoding=utf-8
if has("gui_running")
	let s:uname = system("uname")
	if s:uname == "Darwin\n"
		set guifont=Inconsolata\ for\ Powerline:h15
	endif
endif

"Plugin 'Valloric/YouCompleteMe'

call vundle#end()
filetype plugin indent on " Re-enable filetype scripts
                          " with plugin indentation

syntax on
